package com.agiletestingalliance;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

public class MinMaxTest {

private MinMax minMax;

    @Before
    public void setUp() {
        minMax = new MinMax();
    }

    public int max(int valueA, int valueB) {
        if (valueB > valueA) {
            return valueB;
	}
        else {
            return valueA;
	}
    }

    @Test
    public void testMaxAIsMax() throws Exception {
	int valueA = 9;
        int valueB = 8; 
	int result = minMax.max(valueA,valueB);
        assertEquals("Result", valueA, result);
    }

    @Test
    public void testMaxBIsMax() throws Exception {
	int valueA = 1;
        int valueB = 8; 
	int result = minMax.max(valueA,valueB);
        assertEquals("Result", valueB, result);
    }

}
