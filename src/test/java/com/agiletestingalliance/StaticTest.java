package com.agiletestingalliance;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

public class StaticTest {

private Usefulness usefulness;
private Duration duration;
private AboutCPDOF aboutCPDOF;


    @Before
    public void setUp() {
        usefulness = new Usefulness();
        duration = new Duration();
        aboutCPDOF = new AboutCPDOF();
    }

    @Test
    public void testUsefulnessDess() throws Exception {
	String expecteRes = "DevOps is about transformation, about building quality in";
       
	String result = usefulness.desc();
        assertTrue("Result", result.contains(expecteRes));
    }

    @Test
    public void testDurationDur() throws Exception {
	String expecteRes = "CP-DOF is designed specifically for corporates";
       
	String result = duration.dur();
        assertTrue("Result", result.contains(expecteRes));
    }

    @Test
    public void testAboutCPDOFDess() throws Exception {
	String expecteRes = "CP-DOF certification program covers end to end DevOps Life Cycle practically. ";
       
	String result = aboutCPDOF.desc();
        assertTrue("Result", result.contains(expecteRes));
    }
  
}


