package com.agiletestingalliance;

public class MinMax {

    public int max(int valueA, int valueB) {
        if (valueB > valueA) {
            return valueB;
	}
        else {
            return valueA;
	}
    }

}
